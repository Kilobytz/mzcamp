/*
 * Copyright (C) 2020 FloorIsJava
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.mzcamp.mzcamp;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Objects;

/**
 * Custom items.
 */
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public final class CustomItem {

    /**
     * Lime dye used to cure infection in others.
     */
    public static final CustomItem ANTIBIOTICS = new CustomItem(Material.LIME_DYE, "§6Antibiotics");

    /**
     * Paper bandage. Heals self and others, stops bleeding.
     */
    public static final CustomItem BANDAGE = new CustomItem(Material.PAPER, "§6Bandage");

    /**
     * Iron helmet that slows attackers.
     */
    public static final CustomItem BINDING_HELMET = new CustomItem(Material.IRON_HELMET, "§6Binding Helmet");

    /**
     * Sugar with interesting side effects.
     */
    public static final CustomItem COCAINE = new CustomItem(Material.SUGAR, "§6Cocaine");

    /**
     * Sword that deals more damage to players wielding diamond swords.
     */
    public static final CustomItem CORSAIRS_EDGE = new CustomItem(Material.IRON_SWORD, "§6Corsair's Edge");

    /**
     * Slime ball flash grenades. Hinder sight by smoke and effects.
     */
    public static final CustomItem FLASH_GRENADE = new CustomItem(Material.SLIME_BALL, "§6Flash Grenade");

    /**
     * Ender pearl grenades. Explode on impact.
     */
    public static final CustomItem GRENADE = new CustomItem(Material.ENDER_PEARL, "§6Grenade");

    /**
     * Red dye used to effectively heal players.
     */
    public static final CustomItem HEALING_OINTMENT = new CustomItem(Material.RED_DYE, "§6Healing Ointment");

    /**
     * Wooden sword that spams 0 damage animations at the target.
     */
    public static final CustomItem HURTER = new CustomItem(Material.WOODEN_SWORD, "§6Hurter");

    /**
     * Iron sword that heals on right click.
     */
    public static final CustomItem LONE_SWORD = new CustomItem(Material.IRON_SWORD, "§6Lone Sword");

    /**
     * Iron sword that has a chance to make the target hungry.
     */
    public static final CustomItem MURAMASA = new CustomItem(Material.IRON_SWORD, "§6Muramasa");

    /**
     * Chainmail boots that give speed when being hit.
     */
    public static final CustomItem NINJA_SANDALS = new CustomItem(Material.CHAINMAIL_BOOTS, "§6Ninja Sandals");

    /**
     * Iron chestplate that sometimes heals the wearer when being hit.
     */
    public static final CustomItem PLUVIAS_TEMPEST = new CustomItem(Material.IRON_CHESTPLATE, "§6Pluvia's Tempest");

    /**
     * Bow that shoots 7 arrows for each shot.
     */
    public static final CustomItem SHOT_BOW = new CustomItem(Material.BOW, "§6Shot Bow");

    /**
     * Iron sword that has a chance to restore health when used to hit.
     */
    public static final CustomItem VAMPYR = new CustomItem(Material.IRON_SWORD, "§6Vampyr");

    /**
     * Fishing rod grapple. Pulls user to hook on retract.
     */
    public static final CustomItem WEAK_GRAPPLE = new CustomItem(Material.FISHING_ROD, "§6Weak Grapple");

    /**
     * The material of the item.
     */
    private final Material material;

    /**
     * The custom name of the item.
     */
    private final String customName;

    /**
     * Creates a stack of this item.
     *
     * @return The created stack.
     */
    public ItemStack createStack() {
        final ItemStack result = new ItemStack(material);
        final ItemMeta meta = Objects.requireNonNull(result.getItemMeta());
        meta.setDisplayName(customName);
        result.setItemMeta(meta);
        return result;
    }

    /**
     * Checks if the given item is similar to this.
     *
     * @param item The item.
     * @return {@code true} if the item is similar.
     */
    public boolean isItem(final ItemStack item) {
        if (item == null || item.getType() != material) {
            return false;
        }

        final ItemMeta meta = Objects.requireNonNull(item.getItemMeta());
        return meta.hasDisplayName() && meta.getDisplayName().equals(customName);
    }
}
