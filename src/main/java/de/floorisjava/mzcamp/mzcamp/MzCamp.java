/*
 * Copyright (C) 2020 FloorIsJava
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.mzcamp.mzcamp;

import de.floorisjava.mc.plugin.base.Modular;
import de.floorisjava.mc.plugin.base.module.Module;
import de.floorisjava.mc.plugin.base.module.ModuleManager;
import de.floorisjava.mc.plugin.base.util.EntryPoint;
import de.floorisjava.mzcamp.mzcamp.modules.armor.ArmorModule;
import de.floorisjava.mzcamp.mzcamp.modules.armory.ArmoryModule;
import de.floorisjava.mzcamp.mzcamp.modules.bleeding.BleedingModule;
import de.floorisjava.mzcamp.mzcamp.modules.butcher.ButcherModule;
import de.floorisjava.mzcamp.mzcamp.modules.consumable.ConsumableModule;
import de.floorisjava.mzcamp.mzcamp.modules.death.DeathModule;
import de.floorisjava.mzcamp.mzcamp.modules.fly.FlyModule;
import de.floorisjava.mzcamp.mzcamp.modules.heal.HealModule;
import de.floorisjava.mzcamp.mzcamp.modules.infection.InfectionModule;
import de.floorisjava.mzcamp.mzcamp.modules.kit.KitModule;
import de.floorisjava.mzcamp.mzcamp.modules.login.LoginModule;
import de.floorisjava.mzcamp.mzcamp.modules.logout.LogoutModule;
import de.floorisjava.mzcamp.mzcamp.modules.protection.ProtectionModule;
import de.floorisjava.mzcamp.mzcamp.modules.pve.PveModule;
import de.floorisjava.mzcamp.mzcamp.modules.pvp.PvpModule;
import de.floorisjava.mzcamp.mzcamp.modules.soundlevel.SoundLevelModule;
import de.floorisjava.mzcamp.mzcamp.modules.thirst.ThirstModule;
import de.floorisjava.mzcamp.mzcamp.modules.tweaks.TweaksModule;
import de.floorisjava.mzcamp.mzcamp.modules.ui.UiModule;
import de.floorisjava.mzcamp.mzcamp.modules.weapon.WeaponModule;
import de.floorisjava.mzcamp.mzcamp.modules.worldsetup.WorldSetupModule;
import de.floorisjava.mzcamp.mzcamp.modules.zombies.ZombieModule;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 * Main plugin entry point.
 */
@EntryPoint
public class MzCamp extends JavaPlugin implements ModuleManager {

    /**
     * The list of enabled modules.
     */
    private final List<Module> modules = new ArrayList<>();

    @Override
    public void onEnable() {
        Modular.requireNoexcept(() -> {
            initializeConfig();

            getLogger().info("Plugin logging level: " + getEffectiveLogLevel());
            getLogger().info("Initializing all modules");

            // Create modules here.
            modules.add(new ArmorModule(this));
            modules.add(new ArmoryModule(this));
            modules.add(new BleedingModule(this));
            modules.add(new ButcherModule(this));
            modules.add(new ConsumableModule(this));
            modules.add(new DeathModule(this));
            modules.add(new FlyModule(this));
            modules.add(new HealModule(this));
            modules.add(new InfectionModule(this));
            modules.add(new KitModule(this));
            modules.add(new LoginModule(this));
            modules.add(new LogoutModule(this));
            modules.add(new ProtectionModule(this));
            modules.add(new PveModule(this));
            modules.add(new PvpModule(this));
            modules.add(new SoundLevelModule(this));
            modules.add(new ThirstModule(this));
            modules.add(new TweaksModule(this));
            modules.add(new UiModule(this));
            modules.add(new WeaponModule(this));
            modules.add(new WorldSetupModule(this));
            modules.add(new ZombieModule(this));

            getLogger().info("Initialized all modules");

            getLogger().info("Starting all modules");
            modules.forEach(m -> Modular.requireNoexcept(m::start));
            getLogger().info("Started all modules");
        });
    }

    @Override
    public void onDisable() {
        getLogger().info("Stopping all modules");
        modules.forEach(m -> Modular.requireNoexcept(m::stop));
        modules.clear();
        getLogger().info("Stopped all modules");
    }

    /**
     * Initializes the config.
     */
    private void initializeConfig() {
        // First, save the default config, then (re)load it immediately, then save missing defaults to disk, then reload.
        saveDefaultConfig();
        reloadConfig();
        getConfig().options().copyDefaults(true);
        saveConfig();
        reloadConfig();
    }

    @Override
    public <T> T get(final Class<T> clazz) {
        return modules.stream().filter(clazz::isInstance).map(clazz::cast).findAny().orElse(null);
    }

    /**
     * Obtains the effective log level.
     *
     * @return The effective log level, as a string.
     */
    private String getEffectiveLogLevel() {
        Logger cursor = getLogger();
        while (cursor.getLevel() == null) {
            cursor = cursor.getParent();
            if (cursor == null) {
                return "UNKNOWN";
            }
        }
        return cursor.getLevel().getName();
    }
}
