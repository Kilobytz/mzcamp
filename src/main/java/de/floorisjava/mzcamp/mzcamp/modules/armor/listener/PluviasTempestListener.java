/*
 * Copyright (C) 2020 FloorIsJava
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.mzcamp.mzcamp.modules.armor.listener;

import de.floorisjava.mc.plugin.base.util.PlayerUtil;
import de.floorisjava.mzcamp.mzcamp.CustomItem;
import org.bukkit.Particle;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.inventory.ItemStack;

import java.util.Objects;
import java.util.Random;

/**
 * Implements pluvias tempest functionality.
 */
public class PluviasTempestListener implements Listener {

    /**
     * A random number generator.
     */
    private final Random random = new Random();

    @EventHandler
    public void onEntityDamageByEntity(final EntityDamageByEntityEvent event) {
        if (!(event.getEntity() instanceof Player) || !(event.getDamager() instanceof Player)) {
            return;
        }
        final LivingEntity attacked = (LivingEntity) event.getEntity();

        final ItemStack chestPlate = Objects.requireNonNull(attacked.getEquipment()).getChestplate();
        if (!CustomItem.PLUVIAS_TEMPEST.isItem(chestPlate)) {
            return;
        }

        if (random.nextInt(4) == 0) {
            PlayerUtil.heal((Player) attacked, 1.0);
            attacked.getWorld().spawnParticle(Particle.VILLAGER_HAPPY, attacked.getEyeLocation(), 20, 0.5, 0.5, 0.5);
        }
    }
}
