/*
 * Copyright (C) 2020 FloorIsJava
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.mzcamp.mzcamp.modules.armory;

import de.floorisjava.mzcamp.mzcamp.CustomItem;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.potion.PotionData;
import org.bukkit.potion.PotionType;

import java.util.Collection;
import java.util.HashSet;
import java.util.Objects;
import java.util.UUID;

/**
 * Provides the armory.
 */
public class ArmoryManager {

    /**
     * Contains UUIDs of players in the armory.
     */
    private final Collection<UUID> inArmory = new HashSet<>();

    /**
     * Marks the armory as closed, if open, for the player with the given UUID.
     *
     * @param who The player.
     */
    public void closeArmory(final UUID who) {
        inArmory.remove(who);
    }

    /**
     * Checks if the player with the given UUID is in the armory.
     *
     * @param who The UUID.
     * @return {@code true} if the player is in the armory.
     */
    public boolean isInArmory(final UUID who) {
        return inArmory.contains(who);
    }

    /**
     * Opens the armory for the given player.
     *
     * @param who The player.
     */
    public void openArmory(final Player who) {
        inArmory.add(who.getUniqueId());

        /* PRIORITIES:
        ipse folly
        flail
        robbers blade
        other stuff idk
         */
        final Inventory armory = Bukkit.createInventory(null, 54, "§4§lArmory");
        armory.addItem(
                new ItemStack(Material.BREAD, 3),
                new ItemStack(Material.MELON_SLICE, 3),
                new ItemStack(Material.COOKIE, 8),
                new ItemStack(Material.GOLDEN_APPLE),
                new ItemStack(Material.COBWEB),
                new ItemStack(Material.DIAMOND_SWORD),
                new ItemStack(Material.IRON_HELMET),
                new ItemStack(Material.IRON_CHESTPLATE),
                new ItemStack(Material.IRON_LEGGINGS),
                new ItemStack(Material.IRON_BOOTS),
                CustomItem.BINDING_HELMET.createStack(),
                CustomItem.PLUVIAS_TEMPEST.createStack(),
                CustomItem.NINJA_SANDALS.createStack(),
                CustomItem.LONE_SWORD.createStack(),
                namedItem(Material.WOODEN_SWORD, "§6Robbers Blade - DEFUNCT"), // TODO
                namedItem(Material.GOLDEN_SWORD, "§6Ipse Folly - DEFUNCT"), // TODO
                namedItem(Material.GOLDEN_SWORD, "§6Flail - DEFUNCT"), // TODO
                CustomItem.HURTER.createStack(),
                CustomItem.VAMPYR.createStack(),
                CustomItem.MURAMASA.createStack(),
                CustomItem.CORSAIRS_EDGE.createStack(),
                namedItem(Material.BOW, "§6Binding Bow - DEFUNCT"), // TODO
                namedItem(Material.BOW, "§6Heal Bow - DEFUNCT"), // TODO
                CustomItem.SHOT_BOW.createStack(),
                namedItem(Material.BOW, "§6Void Bow - DEFUNCT"), // TODO
                namedItem(Material.BOW, "§6Zombie Bow - DEFUNCT"), // TODO
                CustomItem.GRENADE.createStack(),
                CustomItem.FLASH_GRENADE.createStack(),
                new ItemStack(Material.SHEARS),
                CustomItem.BANDAGE.createStack(),
                CustomItem.HEALING_OINTMENT.createStack(),
                CustomItem.ANTIBIOTICS.createStack(),
                CustomItem.COCAINE.createStack(),
                CustomItem.WEAK_GRAPPLE.createStack(),
                potion(PotionType.INSTANT_HEAL, true, true),
                potion(PotionType.INSTANT_HEAL, true, false),
                potion(PotionType.SLOWNESS, false, true),
                potion(PotionType.WEAKNESS, false, true),
                potion(PotionType.WATER, false, false),
                potion(PotionType.SPEED, true, false),
                new ItemStack(Material.MILK_BUCKET),
                new ItemStack(Material.BOW),
                new ItemStack(Material.ARROW, 15));

        final ItemStack respirationHelmet = new ItemStack(Material.IRON_HELMET);
        respirationHelmet.addEnchantment(Enchantment.OXYGEN, 1);
        armory.addItem(respirationHelmet);

        final ItemStack depthLeggings = new ItemStack(Material.IRON_LEGGINGS);
        depthLeggings.addUnsafeEnchantment(Enchantment.DEPTH_STRIDER, 1);
        armory.addItem(depthLeggings);

        final ItemStack infBow = new ItemStack(Material.BOW);
        infBow.addEnchantment(Enchantment.ARROW_INFINITE, 1);
        armory.addItem(infBow);

        final ItemStack power2Punch1Bow = new ItemStack(Material.BOW);
        power2Punch1Bow.addEnchantment(Enchantment.ARROW_DAMAGE, 2);
        power2Punch1Bow.addEnchantment(Enchantment.ARROW_KNOCKBACK, 1);
        armory.addItem(power2Punch1Bow);

        final ItemStack power2Bow = new ItemStack(Material.BOW);
        power2Bow.addEnchantment(Enchantment.ARROW_DAMAGE, 2);
        armory.addItem(power2Bow);

        final ItemStack power1Punch1Bow = new ItemStack(Material.BOW);
        power1Punch1Bow.addEnchantment(Enchantment.ARROW_DAMAGE, 1);
        power1Punch1Bow.addEnchantment(Enchantment.ARROW_KNOCKBACK, 1);
        armory.addItem(power1Punch1Bow);

        final ItemStack power1Bow = new ItemStack(Material.BOW);
        power1Bow.addEnchantment(Enchantment.ARROW_DAMAGE, 1);
        armory.addItem(power1Bow);

        final ItemStack kbSword = namedItem(Material.IRON_SWORD, "§6Simoons Song");
        kbSword.addUnsafeEnchantment(Enchantment.KNOCKBACK, 10);
        armory.addItem(kbSword);

        who.openInventory(armory);
    }

    /**
     * Creates an item with name.
     *
     * @param material The material.
     * @param name     The item name.
     * @return The item.
     */
    private ItemStack namedItem(final Material material, final String name) {
        final ItemStack result = new ItemStack(material);
        final ItemMeta meta = Objects.requireNonNull(result.getItemMeta());
        meta.setDisplayName(name);
        result.setItemMeta(meta);
        return result;
    }

    /**
     * Creates a potion.
     *
     * @param type     The potion type.
     * @param upgraded Whether the potion is upgraded.
     * @param splash   Whether it is a splash potion.
     * @return The item.
     */
    private ItemStack potion(final PotionType type, final boolean upgraded, final boolean splash) {
        final ItemStack result = new ItemStack(splash ? Material.SPLASH_POTION : Material.POTION);
        final PotionMeta meta = (PotionMeta) Objects.requireNonNull(result.getItemMeta());
        meta.setBasePotionData(new PotionData(type, false, upgraded));
        result.setItemMeta(meta);
        return result;
    }
}
