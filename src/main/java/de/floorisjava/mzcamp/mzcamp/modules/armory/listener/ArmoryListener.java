/*
 * Copyright (C) 2020 FloorIsJava
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.mzcamp.mzcamp.modules.armory.listener;

import de.floorisjava.mzcamp.mzcamp.modules.armory.ArmoryManager;
import lombok.RequiredArgsConstructor;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;

/**
 * Listens to armory inventory events.
 */
@RequiredArgsConstructor
public class ArmoryListener implements Listener {

    /**
     * The armory manager.
     */
    private final ArmoryManager armoryManager;

    @EventHandler
    public void onInventoryClose(final InventoryCloseEvent event) {
        armoryManager.closeArmory(event.getPlayer().getUniqueId());
    }

    @EventHandler
    public void onPlayerQuit(final PlayerQuitEvent event) {
        armoryManager.closeArmory(event.getPlayer().getUniqueId());
    }

    @EventHandler
    public void onInventoryClick(final InventoryClickEvent event) {
        if (armoryManager.isInArmory(event.getWhoClicked().getUniqueId())) {
            if (event.isShiftClick()) {
                event.setCancelled(true);
            }
            if (event.getClickedInventory() == event.getView().getTopInventory()) {
                event.setCancelled(true);
                final ItemStack current = event.getCurrentItem();
                if (current == null || current.getType() == Material.AIR) {
                    event.getWhoClicked().setItemOnCursor(null);
                } else {
                    if (event.getWhoClicked().getItemOnCursor().getType() == Material.AIR) {
                        event.getWhoClicked().setItemOnCursor(current.clone());
                    }
                }
            }
        }
    }

    @EventHandler
    public void onInventoryDrag(final InventoryDragEvent event) {
        if (armoryManager.isInArmory(event.getWhoClicked().getUniqueId())) {
            for (final int rawSlot : event.getRawSlots()) {
                if (event.getView().getInventory(rawSlot) == event.getView().getTopInventory()) {
                    event.setCancelled(true);
                    return;
                }
            }
        }
    }
}
