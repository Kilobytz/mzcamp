/*
 * Copyright (C) 2020 FloorIsJava
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.mzcamp.mzcamp.modules.bleeding;

import de.floorisjava.mc.plugin.base.module.BaseModule;
import de.floorisjava.mc.plugin.base.module.ModuleManager;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.UUID;

/**
 * Handles damage bleeding.
 */
public class BleedingModule extends BaseModule implements Bleeding, Listener {

    /**
     * Random number generator.
     */
    private final Random random = new Random();

    /**
     * The currently bleeding players, mapped to their task ID.
     */
    private final Map<UUID, Integer> bleedingPlayers = new HashMap<>();

    /**
     * Constructor.
     *
     * @param moduleManager The owning module manager.
     */
    public BleedingModule(final ModuleManager moduleManager) {
        super(moduleManager);

        addListener(() -> this);
    }

    @Override
    protected void stopModule() {
        // This will stop tasks from recurring.
        bleedingPlayers.clear();
    }

    @Override
    public void startBleeding(final Player who) {
        if (!bleedingPlayers.containsKey(who.getUniqueId())) {
            // Sentinel value to pass first check...
            bleedingPlayers.put(who.getUniqueId(), -1);
            bleedingTick(who);
        }
    }

    @Override
    public boolean isBleeding(final Player who) {
        return bleedingPlayers.containsKey(who.getUniqueId());
    }

    @Override
    public void stopBleeding(final Player who, final boolean suppressMessage) {
        if (bleedingPlayers.containsKey(who.getUniqueId())) {
            getModuleManager().getServer().getScheduler().cancelTask(bleedingPlayers.remove(who.getUniqueId()));
            if (!suppressMessage) {
                who.sendMessage("§aThat should stop the bleeding for now...");
            }
        }
    }

    @EventHandler
    public void onEntityDamage(final EntityDamageEvent event) {
        if (event.getEntity() instanceof Player) {
            if (random.nextInt(100) == 0 && event.getFinalDamage() > 0.01) {
                startBleeding((Player) event.getEntity());
            }
        }
    }

    @EventHandler
    public void onPlayerDeath(final PlayerDeathEvent event) {
        getModuleManager().getServer().getScheduler().scheduleSyncDelayedTask(getModuleManager(),
                () -> stopBleeding(event.getEntity(), true));
    }

    /**
     * Applies bleeding effects to the given player.
     *
     * @param who The player.
     */
    private void bleedingTick(final Player who) {
        if (!bleedingPlayers.containsKey(who.getUniqueId())) {
            return;
        }

        if (who.getHealth() >= 3) {
            who.damage(2);
        } else if (who.getHealth() >= 2) {
            who.damage(1);
        }

        who.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 100, 1, false, false, false));
        who.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 20, 0, false, false, false));
        who.sendMessage("§4Ouch! I need a bandage.");

        final int taskId = getModuleManager().getServer().getScheduler().scheduleSyncDelayedTask(getModuleManager(),
                () -> bleedingTick(who), random.nextInt(200) + 600);
        bleedingPlayers.put(who.getUniqueId(), taskId);
    }
}
