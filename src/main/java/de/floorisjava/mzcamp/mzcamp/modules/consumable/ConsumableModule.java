/*
 * Copyright (C) 2020 FloorIsJava
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.mzcamp.mzcamp.modules.consumable;

import de.floorisjava.mc.plugin.base.module.BaseModule;
import de.floorisjava.mc.plugin.base.module.ModuleManager;
import de.floorisjava.mzcamp.mzcamp.modules.bleeding.Bleeding;
import de.floorisjava.mzcamp.mzcamp.modules.consumable.listener.BandageListener;
import de.floorisjava.mzcamp.mzcamp.modules.consumable.listener.CocaineListener;
import de.floorisjava.mzcamp.mzcamp.modules.consumable.listener.ContainerRemover;
import de.floorisjava.mzcamp.mzcamp.modules.consumable.listener.FoodInfectionListener;
import de.floorisjava.mzcamp.mzcamp.modules.consumable.listener.FoodListener;
import de.floorisjava.mzcamp.mzcamp.modules.consumable.listener.ParamedicListener;
import de.floorisjava.mzcamp.mzcamp.modules.infection.Infection;

/**
 * Handles consumables.
 */
public class ConsumableModule extends BaseModule {

    /**
     * Interface for bleeding feature interoperability.
     */
    private Bleeding bleeding;

    /**
     * Interface for infection feature interoperability.
     */
    private Infection infection;

    /**
     * Constructor.
     *
     * @param moduleManager The owning module manager.
     */
    public ConsumableModule(final ModuleManager moduleManager) {
        super(moduleManager);

        addListener(() -> new BandageListener(bleeding));
        addListener(() -> new CocaineListener(getModuleManager()));
        addListener(() -> new ContainerRemover(getModuleManager()));
        addListener(() -> new FoodInfectionListener(infection));
        addListener(FoodListener::new);
        addListener(() -> new ParamedicListener(getModuleManager(), bleeding, infection));
    }

    @Override
    protected void startModule() {
        bleeding = getModuleManager().get(Bleeding.class);
        infection = getModuleManager().get(Infection.class);
    }
}
