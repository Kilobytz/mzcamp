/*
 * Copyright (C) 2020 FloorIsJava
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.mzcamp.mzcamp.modules.consumable.listener;

import de.floorisjava.mc.plugin.base.util.ItemUtil;
import de.floorisjava.mc.plugin.base.util.PlayerUtil;
import de.floorisjava.mzcamp.mzcamp.CustomItem;
import de.floorisjava.mzcamp.mzcamp.modules.bleeding.Bleeding;
import lombok.RequiredArgsConstructor;
import org.bukkit.Sound;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;

import java.util.Objects;

/**
 * Implements bandage functionality.
 */
@RequiredArgsConstructor
public class BandageListener implements Listener {

    /**
     * Bleeding feature interface.
     */
    private final Bleeding bleeding;

    @EventHandler
    public void onPlayerInteract(final PlayerInteractEvent event) {
        if (!event.hasItem() || (event.getAction() != Action.RIGHT_CLICK_AIR
                                 && event.getAction() != Action.RIGHT_CLICK_BLOCK)) {
            return;
        }

        if (event.getHand() != EquipmentSlot.HAND) {
            return;
        }

        final ItemStack item = Objects.requireNonNull(event.getItem());
        if (!CustomItem.BANDAGE.isItem(item)) {
            return;
        }

        ItemUtil.consumeOne(item);
        event.getPlayer().getInventory().setItemInMainHand(item);

        event.getPlayer().playSound(event.getPlayer().getLocation(), Sound.ENTITY_SHEEP_SHEAR, 4.0f, 1.0f);
        final double healAmount;
        if (bleeding.isBleeding(event.getPlayer())) {
            healAmount = 1.0;
            bleeding.stopBleeding(event.getPlayer(), false);
        } else {
            healAmount = 2.0;
        }

        PlayerUtil.heal(event.getPlayer(), healAmount);
        event.getPlayer().sendMessage("§aAh, that's much better.");
    }
}
