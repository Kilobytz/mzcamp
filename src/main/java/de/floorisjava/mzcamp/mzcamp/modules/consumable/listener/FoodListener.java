/*
 * Copyright (C) 2020 FloorIsJava
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.mzcamp.mzcamp.modules.consumable.listener;

import de.floorisjava.mc.plugin.base.util.PlayerUtil;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.player.PlayerItemConsumeEvent;

/**
 * Performs food logic.
 */
public class FoodListener implements Listener {

    @EventHandler
    public void onPlayerItemConsume(final PlayerItemConsumeEvent event) {
        switch (event.getItem().getType()) {
            case MILK_BUCKET:
            case POTION:
            case ROTTEN_FLESH:
                break;
            default:
                PlayerUtil.heal(event.getPlayer(), 1.0);
                break;
        }
    }

    @EventHandler
    public void onFoodLevelChange(final FoodLevelChangeEvent event) {
        if (!(event.getEntity() instanceof Player)) {
            return;
        }
        final Player who = (Player) event.getEntity();

        if (event.getItem() != null) {
            final float saturation;
            final int foodChange;
            switch (event.getItem().getType()) {
                case MELON_SLICE:
                    saturation = 1.2f;
                    foodChange = 2;
                    break;
                case BREAD:
                    saturation = 6.0f;
                    foodChange = 5;
                    break;
                case COOKIE:
                    saturation = 0.6f;
                    foodChange = 2;
                    break;
                default:
                    return;
            }

            event.setFoodLevel(Math.min(20, who.getFoodLevel() + foodChange));
            who.setSaturation(who.getSaturation() + saturation);
        }
    }
}
