/*
 * Copyright (C) 2020 FloorIsJava
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.mzcamp.mzcamp.modules.consumable.listener;

import de.floorisjava.mc.plugin.base.util.Cooldown;
import de.floorisjava.mzcamp.mzcamp.CustomItem;
import de.floorisjava.mzcamp.mzcamp.modules.bleeding.Bleeding;
import de.floorisjava.mzcamp.mzcamp.modules.infection.Infection;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.attribute.Attribute;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;

/**
 * Implements healing functionality.
 * <p>
 * Not quite "consumable", but where else to put it?
 */
@RequiredArgsConstructor
public class ParamedicListener implements Listener {

    /**
     * The owning plugin, for scheduling.
     */
    private final Plugin plugin;

    /**
     * Bleeding feature interface.
     */
    private final Bleeding bleeding;

    /**
     * Infection feature interface.
     */
    private final Infection infection;

    /**
     * Cooldown for players to be healed.
     */
    private final Cooldown<UUID> healCooldown = new Cooldown<>();

    /**
     * All current healing attempts.
     */
    private final Map<UUID, HealingAttempt> healingAttempts = new HashMap<>();

    @EventHandler
    public void onEntityDamageByEntity(final EntityDamageByEntityEvent event) {
        if (!(event.getDamager() instanceof Player) || !(event.getEntity() instanceof Player)) {
            return;
        }
        final Player healer = (Player) event.getDamager();
        final Player patient = (Player) event.getEntity();

        final ItemStack clickItem = ((Player) event.getDamager()).getInventory().getItemInMainHand();
        if (clickItem.getType() == Material.SHEARS) {
            event.setCancelled(true);
            if (isHealing(healer, patient)) {
                finishHealing(healer, patient);
            } else {
                performDiagnosis(healer, patient);
            }
        }

        if (CustomItem.BANDAGE.isItem(clickItem)) {
            event.setCancelled(true);
            beginHealing(healer, patient);
        }

        if (CustomItem.HEALING_OINTMENT.isItem(clickItem)) {
            event.setCancelled(true);
            applyOintment(healer, patient);
        }

        if (CustomItem.ANTIBIOTICS.isItem(clickItem)) {
            event.setCancelled(true);
            applyAntibiotics(healer, patient);
        }
    }

    /**
     * Shows the healer a diagnosis of the patient.
     *
     * @param healer  The healer.
     * @param patient The patient.
     */
    private void performDiagnosis(final Player healer, final Player patient) {
        healer.sendMessage(String.format("§e%1$s is at %2$.1f hearts.", patient.getName(), patient.getHealth() / 2.0));
        if (bleeding.isBleeding(patient)) {
            healer.sendMessage(String.format("§e%1$s is bleeding.", patient.getName()));
        }

        if (healCooldown.isOnCooldown(patient.getUniqueId())) {
            final long secondsLeft = healCooldown.getRemainingCooldown(patient.getUniqueId()) / 1000;
            healer.sendMessage(String.format("§cYou must wait %1$d seconds before healing %2$s.", secondsLeft,
                    patient.getName()));
        } else {
            final double maxHealth =
                    Objects.requireNonNull(patient.getAttribute(Attribute.GENERIC_MAX_HEALTH)).getValue();
            if (maxHealth - patient.getHealth() > 0.1) {
                healer.sendMessage(String.format("§aYou are able to heal %1$s.", patient.getName()));
            }
        }
    }

    /**
     * Checks whether the given player is currently healing the patient.
     *
     * @param healer  The healer.
     * @param patient The patient.
     * @return {@code true} if the player is healing the patient.
     */
    private boolean isHealing(final Player healer, final Player patient) {
        final HealingAttempt attempt = healingAttempts.get(healer.getUniqueId());
        return attempt != null && attempt.getReceiver().equals(patient.getUniqueId());
    }

    /**
     * Causes the healer to begin healing the patient.
     *
     * @param healer  The healer.
     * @param patient The patient.
     */
    private void beginHealing(final Player healer, final Player patient) {
        if (!healingAttempts.containsKey(healer.getUniqueId())) {
            if (healCooldown.isOnCooldown(patient.getUniqueId())) {
                healer.sendMessage(String.format("§cYou must wait %1$d seconds until you can heal %2$s.",
                        healCooldown.getRemainingCooldown(patient.getUniqueId()) / 1000, patient.getName()));
            } else {
                final HealingAttempt attempt = new HealingAttempt(patient.getUniqueId());
                healingAttempts.put(healer.getUniqueId(), attempt);
                healer.sendMessage(String.format("§aYou started bandaging %1$s.", patient.getName()));
                patient.sendMessage(String.format("§a%1$s started bandaging you.", healer.getName()));
                plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin,
                        () -> failToHeal(healer, patient), 100L);
            }
        } else {
            healer.sendMessage("§cYou have already begun bandaging a player.");
        }
    }

    /**
     * Causes the healer to finish healing the patient.
     *
     * @param healer  The healer.
     * @param patient The patient.
     */
    private void finishHealing(final Player healer, final Player patient) {
        if (!isHealing(healer, patient)) {
            return;
        }
        final HealingAttempt attempt = healingAttempts.get(healer.getUniqueId());
        attempt.apply(patient);
        healer.sendMessage(String.format("§aYou successfully bandaged %1$s's wounds.", patient.getName()));
        patient.sendMessage(String.format("§aYou were successfully bandaged by %1$s.", healer.getName()));
        healCooldown.addCooldown(patient.getUniqueId(), 5 * 60 * 1000);
        healingAttempts.remove(healer.getUniqueId());
    }

    /**
     * Causes the healer to apply ointment to the patient.
     *
     * @param healer  The healer.
     * @param patient The patient.
     */
    private void applyOintment(final Player healer, final Player patient) {
        if (!isHealing(healer, patient)) {
            return;
        }
        final HealingAttempt attempt = healingAttempts.get(healer.getUniqueId());

        if (attempt.isOintmentApplied()) {
            healer.sendMessage("§cYou already applied this.");
        } else {
            attempt.setOintmentApplied(true);
            healer.sendMessage(String.format("§aYou applied a healing ointment to %1$s's wounds.", patient.getName()));
            patient.sendMessage(String.format("§a%1$s applied a healing ointment to your wounds.", healer.getName()));
        }
    }

    /**
     * Causes the healer to apply antibiotics to the patient.
     *
     * @param healer  The healer.
     * @param patient The patient.
     */
    private void applyAntibiotics(final Player healer, final Player patient) {
        if (!isHealing(healer, patient)) {
            return;
        }
        final HealingAttempt attempt = healingAttempts.get(healer.getUniqueId());

        if (attempt.isAntibioticsApplied()) {
            healer.sendMessage("§cYou already applied this.");
        } else {
            attempt.setAntibioticsApplied(true);
            healer.sendMessage(String.format("§aYou applied antibiotics to %1$s's wounds.", patient.getName()));
            patient.sendMessage(String.format("§a%1$s applied antibiotics to your wounds.", healer.getName()));
        }
    }

    /**
     * Causes the healer to fail healing the patient.
     *
     * @param healer  The healer.
     * @param patient The patient.
     */
    private void failToHeal(final Player healer, final Player patient) {
        final HealingAttempt attempt = healingAttempts.get(healer.getUniqueId());
        if (attempt != null && attempt.getReceiver().equals(patient.getUniqueId())) {
            healer.sendMessage(String.format("§cYou failed to heal %1$s.", patient.getName()));
            patient.sendMessage(String.format("§c%1$s failed to heal you.", healer.getName()));
            healingAttempts.remove(healer.getUniqueId());
        }
    }

    /**
     * Represents the state of a healing attempt.
     */
    @Getter
    @RequiredArgsConstructor
    @Setter
    private class HealingAttempt {

        /**
         * The receiver of the healing.
         */
        private final UUID receiver;

        /**
         * Whether the healer applied a healing ointment.
         */
        private boolean ointmentApplied;

        /**
         * Whether the healer applied antibiotics.
         */
        private boolean antibioticsApplied;

        /**
         * Applies the healing to the given patient.
         *
         * @param patient The patient.
         */
        public void apply(final Player patient) {
            bleeding.stopBleeding(patient, false);
            if (antibioticsApplied) {
                infection.stopInfection(patient, false);
            }

            final int regenerationTicks = ointmentApplied ? 200 : 100;
            patient.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, regenerationTicks, 1,
                    false, false, false));
            patient.getWorld().spawnParticle(Particle.HEART, patient.getEyeLocation(), 16, 0.25, 0.5, 0.25);
        }
    }
}
