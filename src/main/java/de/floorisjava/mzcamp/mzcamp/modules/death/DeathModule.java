/*
 * Copyright (C) 2020 FloorIsJava
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.mzcamp.mzcamp.modules.death;

import de.floorisjava.mc.plugin.base.module.BaseModule;
import de.floorisjava.mc.plugin.base.module.ModuleManager;
import org.bukkit.World;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Zombie;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.metadata.MetadataValue;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Handles player death.
 */
public class DeathModule extends BaseModule implements Listener {

    /**
     * Constructor.
     *
     * @param moduleManager The owning module manager.
     */
    public DeathModule(final ModuleManager moduleManager) {
        super(moduleManager);

        addListener(() -> this);
    }

    @EventHandler
    public void onPlayerDeath(final PlayerDeathEvent event) {
        if (event.getEntity().getWorld().getNearbyEntities(event.getEntity().getLocation(), 30, 30, 30,
                e -> e.getType() == EntityType.ZOMBIE).size() > 10) {
            return;
        }

        final List<ItemStack> savedDrops = new ArrayList<>();
        for (final ItemStack drop : event.getDrops()) {
            savedDrops.add(drop.clone());
        }
        event.getDrops().clear();

        final Zombie zombie = event.getEntity().getWorld().spawn(event.getEntity().getLocation(), Zombie.class);
        Objects.requireNonNull(zombie.getEquipment())
                .setArmorContents(event.getEntity().getInventory().getArmorContents());

        zombie.setAdult();
        zombie.setRemoveWhenFarAway(false);
        zombie.setHealth(10.0);
        zombie.setCustomName(event.getEntity().getName());
        zombie.setCustomNameVisible(true);
        zombie.setCanPickupItems(true);
        zombie.getEquipment().setBootsDropChance(0.0f);
        zombie.getEquipment().setLeggingsDropChance(0.0f);
        zombie.getEquipment().setChestplateDropChance(0.0f);
        zombie.getEquipment().setHelmetDropChance(0.0f);

        zombie.setMetadata("savedDrops", new FixedMetadataValue(getModuleManager(), savedDrops));
    }

    @EventHandler
    public void onEntityDeath(final EntityDeathEvent event) {
        if (event.getEntityType() != EntityType.ZOMBIE || !event.getEntity().hasMetadata("savedDrops")) {
            return;
        }

        final List<MetadataValue> metadata = event.getEntity().getMetadata("savedDrops");
        for (final MetadataValue value : metadata) {
            if (value.getOwningPlugin() != getModuleManager() || !(value.value() instanceof List<?>)) {
                continue;
            }

            List<?> valueList = (List<?>) value.value();
            if (valueList == null) {
                return;
            }

            final World world = event.getEntity().getWorld();
            for (final Object element : valueList) {
                if (element instanceof ItemStack) {
                    world.dropItemNaturally(event.getEntity().getLocation(), (ItemStack) element);
                }
            }
        }
    }
}
