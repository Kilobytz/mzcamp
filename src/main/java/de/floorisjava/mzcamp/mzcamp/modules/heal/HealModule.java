/*
 * Copyright (C) 2020 FloorIsJava
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.mzcamp.mzcamp.modules.heal;

import de.floorisjava.mc.plugin.base.module.BaseModule;
import de.floorisjava.mc.plugin.base.module.ModuleManager;
import de.floorisjava.mzcamp.mzcamp.modules.bleeding.Bleeding;
import de.floorisjava.mzcamp.mzcamp.modules.heal.command.HealCommand;
import de.floorisjava.mzcamp.mzcamp.modules.infection.Infection;

/**
 * Handles /heal.
 */
public class HealModule extends BaseModule {

    /**
     * The bleeding feature API.
     */
    private Bleeding bleeding;

    /**
     * The infection feature API.
     */
    private Infection infection;

    /**
     * Constructor.
     *
     * @param moduleManager The owning module manager.
     */
    public HealModule(final ModuleManager moduleManager) {
        super(moduleManager);
    }

    @Override
    protected void startModule() {
        bleeding = getModuleManager().get(Bleeding.class);
        infection = getModuleManager().get(Infection.class);
        getModuleManager().getCommand("heal").setExecutor(new HealCommand(bleeding, infection));
    }

    @Override
    protected void stopModule() {
        getModuleManager().getCommand("heal").setExecutor(null);
        bleeding = null;
        infection = null;
    }
}
