/*
 * Copyright (C) 2020 FloorIsJava
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.mzcamp.mzcamp.modules.kit.command;

import de.floorisjava.mc.plugin.base.command.LeafCommand;
import de.floorisjava.mzcamp.mzcamp.modules.kit.model.KitManager;
import org.bukkit.command.CommandSender;

/**
 * /delkit [name]
 */
public class DeleteKitCommand extends LeafCommand {

    /**
     * The kit manager.
     */
    private final KitManager kitManager;

    /**
     * Constructor.
     */
    public DeleteKitCommand(final KitManager kitManager) {
        super("/delkit [name] - Removes a kit", "mzcamp.delkit");
        this.kitManager = kitManager;
    }

    @Override
    protected void invoke(final CommandSender sender, final String[] args) {
        if (args.length != 1) {
            sender.sendMessage("§cPlease provide a kit name!");
            return;
        }

        if (kitManager.kitExists(args[0])) {
            kitManager.deleteKit(args[0]);
            sender.sendMessage("§aKit deleted.");
        } else {
            sender.sendMessage("§cKit does not exist.");
        }
    }
}
