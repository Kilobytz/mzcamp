/*
 * Copyright (C) 2020 FloorIsJava
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.mzcamp.mzcamp.modules.kit.command;

import de.floorisjava.mc.plugin.base.command.LeafCommand;
import de.floorisjava.mzcamp.mzcamp.modules.kit.model.KitManager;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * /kit [name]
 */
public class KitCommand extends LeafCommand {

    /**
     * The kit manager.
     */
    private final KitManager kitManager;

    /**
     * Constructor.
     */
    public KitCommand(final KitManager kitManager) {
        super("/kit [name] [player] - Spawns a kit", "mzcamp.kit");
        this.kitManager = kitManager;
    }

    @Override
    protected void invoke(final CommandSender sender, final String[] args) {
        if (args.length < 1) {
            sender.sendMessage("§cPlease provide a kit name!");
            return;
        }

        if (kitManager.kitExists(args[0])) {
            if (args.length == 2) {
                final Player target = Bukkit.getPlayer(args[1]);
                if (target != null) {
                    kitManager.spawnKit(args[0], target);
                    sender.sendMessage(String.format("§aKit %1$s spawned for player %2$s.", args[0], target.getName()));
                } else {
                    sender.sendMessage(String.format("§cUnknown target player %1$s.", args[1]));
                }
            } else {
                if (!(sender instanceof Player)) {
                    sender.sendMessage("§cOnly for players!");
                    return;
                }

                kitManager.spawnKit(args[0], (Player) sender);
                sender.sendMessage(String.format("§aKit %1$s spawned.", args[0]));
            }
        } else {
            sender.sendMessage("§cUnknown kit!");
        }
    }
}
