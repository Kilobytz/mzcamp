/*
 * Copyright (C) 2020 FloorIsJava
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.mzcamp.mzcamp.modules.kit.command;

import de.floorisjava.mc.plugin.base.command.LeafCommand;
import de.floorisjava.mzcamp.mzcamp.modules.kit.model.KitManager;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 * /setkit [name]
 */
public class SetKitCommand extends LeafCommand {

    /**
     * The kit manager.
     */
    private final KitManager kitManager;

    /**
     * Constructor.
     */
    public SetKitCommand(final KitManager kitManager) {
        super("/setkit [name] - Saves your inventory as a kit", "mzcamp.setkit");
        this.kitManager = kitManager;
    }

    @Override
    protected void invoke(final CommandSender sender, final String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage("§cOnly available for players!");
            return;
        }
        final Player who = (Player) sender;

        if (args.length != 1) {
            sender.sendMessage("§cPlease provide a kit name!");
            return;
        }

        List<ItemStack> items = new ArrayList<>();
        Arrays.stream(who.getInventory().getContents())
                .filter(Objects::nonNull)
                .filter(x -> x.getType() != Material.AIR)
                .map(ItemStack::clone)
                .forEach(items::add);
        kitManager.createKit(args[0], items);
        sender.sendMessage(String.format("§aKit %1$s created.", args[0]));
    }
}
