/*
 * Copyright (C) 2020 FloorIsJava
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.mzcamp.mzcamp.modules.kit.model;

import de.floorisjava.mc.plugin.base.util.EntryPoint;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.configuration.serialization.SerializableAs;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A collection of items the can be spawned.
 */
@SerializableAs("de.floorisjava.mzcamp.mzcamp.serial.Kit")
public class Kit implements ConfigurationSerializable {

    /**
     * The items in the kit.
     */
    private final List<ItemStack> items = new ArrayList<>();

    /**
     * Deserialization constructor.
     *
     * @param serial The serialized object.
     */
    @EntryPoint
    public Kit(final Map<String, Object> serial) {
        final Object serialItems = serial.get("items");
        if (serialItems instanceof List<?>) {
            final List<?> serialList = (List<?>) serial.get("items");
            serialList.stream()
                    .filter(ItemStack.class::isInstance)
                    .map(ItemStack.class::cast)
                    .forEach(items::add);
        }
    }

    /**
     * Constructor.
     *
     * @param items The items for the kit.
     */
    public Kit(final Collection<ItemStack> items) {
        this.items.addAll(items);
    }

    @Override
    public Map<String, Object> serialize() {
        final Map<String, Object> result = new HashMap<>();
        result.put("items", items);
        return result;
    }

    /**
     * Spawns the kit for the given player.
     *
     * @param who The player.
     */
    public void spawn(final Player who) {
        items.forEach(i -> {
            switch (i.getType()) {
                case LEATHER_HELMET:
                case CHAINMAIL_HELMET:
                case IRON_HELMET:
                case GOLDEN_HELMET:
                case DIAMOND_HELMET:
                case NETHERITE_HELMET:
                    if (who.getInventory().getHelmet() == null) {
                        who.getInventory().setHelmet(i);
                        return;
                    }
                    break;
                case LEATHER_CHESTPLATE:
                case CHAINMAIL_CHESTPLATE:
                case IRON_CHESTPLATE:
                case GOLDEN_CHESTPLATE:
                case DIAMOND_CHESTPLATE:
                case NETHERITE_CHESTPLATE:
                    if (who.getInventory().getChestplate() == null) {
                        who.getInventory().setChestplate(i);
                        return;
                    }
                    break;
                case LEATHER_LEGGINGS:
                case CHAINMAIL_LEGGINGS:
                case IRON_LEGGINGS:
                case GOLDEN_LEGGINGS:
                case DIAMOND_LEGGINGS:
                case NETHERITE_LEGGINGS:
                    if (who.getInventory().getLeggings() == null) {
                        who.getInventory().setLeggings(i);
                        return;
                    }
                    break;
                case LEATHER_BOOTS:
                case CHAINMAIL_BOOTS:
                case IRON_BOOTS:
                case GOLDEN_BOOTS:
                case DIAMOND_BOOTS:
                case NETHERITE_BOOTS:
                    if (who.getInventory().getBoots() == null) {
                        who.getInventory().setBoots(i);
                        return;
                    }
                    break;
            }

            final Map<Integer, ItemStack> remainder = who.getInventory().addItem(i);
            for (final ItemStack item : remainder.values()) {
                who.getWorld().dropItem(who.getLocation(), item);
            }
        });
    }
}
