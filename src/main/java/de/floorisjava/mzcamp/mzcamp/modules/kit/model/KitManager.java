/*
 * Copyright (C) 2020 FloorIsJava
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.mzcamp.mzcamp.modules.kit.model;

import de.floorisjava.mc.plugin.base.util.EntryPoint;
import lombok.NoArgsConstructor;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.configuration.serialization.SerializableAs;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Manages {@link Kit}s.
 */
@NoArgsConstructor
@SerializableAs("de.floorisjava.mzcamp.mzcamp.serial.KitManager")
public class KitManager implements ConfigurationSerializable {

    /**
     * The registered kits.
     */
    private final Map<String, Kit> kits = new HashMap<>();

    /**
     * Deserialization constructor.
     *
     * @param serial The serialized object.
     */
    @EntryPoint
    public KitManager(final Map<String, Object> serial) {
        final Object serialKits = serial.get("kits");
        if (serialKits instanceof Map<?, ?>) {
            final Map<?, ?> serialMap = (Map<?, ?>) serialKits;
            for (final Map.Entry<?, ?> entry : serialMap.entrySet()) {
                if (entry.getKey() instanceof String && entry.getValue() instanceof Kit) {
                    kits.put((String) entry.getKey(), (Kit) entry.getValue());
                }
            }
        }
    }

    /**
     * Obtains all available kit names.
     *
     * @return The kit names.
     */
    public Collection<String> getKitNames() {
        return Collections.unmodifiableCollection(kits.keySet());
    }

    @Override
    public Map<String, Object> serialize() {
        final Map<String, Object> result = new HashMap<>();
        result.put("kits", kits);
        return result;
    }

    /**
     * Creates a kit with the given name and items.
     *
     * @param name  The name.
     * @param items The items.
     */
    public void createKit(final String name, final List<ItemStack> items) {
        kits.put(name, new Kit(items));
    }

    /**
     * Checks whether a kit with the given name exists.
     *
     * @param name The name.
     * @return {@code true} if the kit exists.
     */
    public boolean kitExists(final String name) {
        return kits.containsKey(name);
    }

    /**
     * Spawns the kit for the given player.
     *
     * @param name The kit.
     * @param who  The player.
     * @throws IllegalArgumentException If the kit does not exist.
     */
    public void spawnKit(final String name, final Player who) {
        final Kit kit = kits.get(name);
        if (kit == null) {
            throw new IllegalArgumentException("kit does not exist");
        }

        kit.spawn(who);
    }

    /**
     * Deletes the kit with the given name.
     *
     * @param name The name.
     * @throws IllegalArgumentException If the kit does not exist.
     */
    public void deleteKit(final String name) {
        if (kits.remove(name) == null) {
            throw new IllegalArgumentException("kit does not exist");
        }
    }
}
