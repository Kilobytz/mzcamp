/*
 * Copyright (C) 2020 FloorIsJava
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.mzcamp.mzcamp.modules.pve;

import de.floorisjava.mc.plugin.base.module.BaseModule;
import de.floorisjava.mc.plugin.base.module.ModuleManager;
import de.floorisjava.mzcamp.mzcamp.modules.pve.command.PveCommand;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.entity.Mob;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityTargetEvent;

/**
 * Handles /pve.
 */
public class PveModule extends BaseModule implements Listener, Pve {

    /**
     * Whether or not PvE is disabled.
     */
    @Getter
    private boolean pveDisabled;

    /**
     * Constructor.
     *
     * @param moduleManager The owning module manager.
     */
    public PveModule(final ModuleManager moduleManager) {
        super(moduleManager);

        addListener(() -> this);
    }

    @Override
    protected void startModule() {
        getModuleManager().getCommand("pve").setExecutor(new PveCommand(this));
    }

    @Override
    protected void stopModule() {
        getModuleManager().getCommand("pve").setExecutor(null);
    }

    @EventHandler
    public void onEntityTarget(final EntityTargetEvent event) {
        if (pveDisabled) {
            if (event.getTarget() instanceof Player) {
                event.setCancelled(true);
            }
        }
    }

    @Override
    public void setPveDisabled(final boolean state) {
        this.pveDisabled = state;
        if (pveDisabled) {
            Bukkit.getWorlds().forEach(w -> w.getLivingEntities().stream()
                    .filter(Mob.class::isInstance)
                    .map(Mob.class::cast)
                    .filter(m -> m.getTarget() instanceof Player)
                    .forEach(m -> m.setTarget(null)));
        }
    }
}
