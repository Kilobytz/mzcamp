/*
 * Copyright (C) 2020 FloorIsJava
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.mzcamp.mzcamp.modules.pve.command;

import de.floorisjava.mc.plugin.base.command.LeafCommand;
import de.floorisjava.mzcamp.mzcamp.modules.pve.Pve;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;

/**
 * /pve.
 */
public class PveCommand extends LeafCommand {

    /**
     * The module interface.
     */
    private final Pve pve;

    /**
     * Constructor.
     */
    public PveCommand(final Pve pve) {
        super("/pve - Toggles PvE", "mzcamp.pve");
        this.pve = pve;
    }

    @Override
    protected void invoke(final CommandSender sender, final String[] args) {
        pve.setPveDisabled(!pve.isPveDisabled());
        if (pve.isPveDisabled()) {
            Bukkit.broadcastMessage(
                    String.format("§e%1$s has disabled PvE globally. Zombies will no longer target you.",
                            sender.getName()));
        } else {
            Bukkit.broadcastMessage(
                    String.format("§e%1$s has enabled PvE globally. Zombies will once again target you.",
                            sender.getName()));
        }
    }
}
