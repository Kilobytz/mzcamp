/*
 * Copyright (C) 2020 FloorIsJava
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.mzcamp.mzcamp.modules.pvp;

import de.floorisjava.mc.plugin.base.module.ModuleStatus;

/**
 * PvP module functionality.
 */
public interface Pvp extends ModuleStatus {

    /**
     * Returns whether or not PvP is disabled.
     *
     * @return {@code true} if it is disabled.
     */
    boolean isPvpDisabled();

    /**
     * Changes whether or not PvP is disabled.
     *
     * @param state The new state.
     */
    void setPvpDisabled(final boolean state);
}
