/*
 * Copyright (C) 2020 FloorIsJava
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.mzcamp.mzcamp.modules.pvp;

import de.floorisjava.mc.plugin.base.module.BaseModule;
import de.floorisjava.mc.plugin.base.module.ModuleManager;
import de.floorisjava.mzcamp.mzcamp.modules.pvp.command.PvpCommand;
import de.floorisjava.mzcamp.mzcamp.modules.pvp.listener.AttackSpeedListener;
import de.floorisjava.mzcamp.mzcamp.modules.pvp.listener.BowDamageListener;
import de.floorisjava.mzcamp.mzcamp.modules.pvp.listener.PvpListener;
import de.floorisjava.mzcamp.mzcamp.modules.pvp.listener.SwordDamageListener;
import de.floorisjava.mzcamp.mzcamp.modules.pvp.task.IronHelmetBuffTask;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.Bukkit;

/**
 * Handles 1.8-esque pvp.
 */
public class PvpModule extends BaseModule implements Pvp {

    /**
     * Whether or not PvP is disabled.
     */
    @Getter
    @Setter
    private boolean pvpDisabled;

    /**
     * Constructor.
     *
     * @param moduleManager The owning module manager.
     */
    public PvpModule(final ModuleManager moduleManager) {
        super(moduleManager);

        addListener(AttackSpeedListener::new);
        addListener(BowDamageListener::new);
        addListener(() -> new PvpListener(this));
        addListener(SwordDamageListener::new);
    }

    @Override
    protected void startModule() {
        Bukkit.getOnlinePlayers().forEach(AttackSpeedListener::increaseAttackSpeed);
        getModuleManager().getCommand("pvp").setExecutor(new PvpCommand(this));

        addTask(getModuleManager().getServer().getScheduler().scheduleSyncRepeatingTask(getModuleManager(),
                new IronHelmetBuffTask(), 30L, 30L));
    }

    @Override
    protected void stopModule() {
        getModuleManager().getCommand("pvp").setExecutor(null);
    }
}
