/*
 * Copyright (C) 2020 FloorIsJava
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.mzcamp.mzcamp.modules.pvp.task;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.attribute.Attribute;
import org.bukkit.attribute.AttributeModifier;
import org.bukkit.entity.Player;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Collection;
import java.util.Objects;
import java.util.UUID;

/**
 * A task that buffs iron helmets to 3 armor points.
 */
public class IronHelmetBuffTask implements Runnable {

    @Override
    public void run() {
        for (final Player who : Bukkit.getOnlinePlayers()) {
            final ItemStack helmet = who.getInventory().getHelmet();
            if (helmet != null && helmet.getType() == Material.IRON_HELMET) {
                final ItemMeta meta = Objects.requireNonNull(helmet.getItemMeta());
                final Collection<AttributeModifier> modifiers = meta.getAttributeModifiers(Attribute.GENERIC_ARMOR);
                if (modifiers != null && !modifiers.isEmpty()) {
                    return;
                }

                final AttributeModifier modifier = new AttributeModifier(UUID.randomUUID(), "ironHelmetArmor", 3.0,
                        AttributeModifier.Operation.ADD_NUMBER, EquipmentSlot.HEAD);
                meta.addAttributeModifier(Attribute.GENERIC_ARMOR, modifier);
                helmet.setItemMeta(meta);
                who.getInventory().setHelmet(helmet);
            }
        }
    }
}
