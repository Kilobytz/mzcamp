/*
 * Copyright (C) 2020 FloorIsJava
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.mzcamp.mzcamp.modules.ui;

import de.floorisjava.mc.plugin.base.module.BaseModule;
import de.floorisjava.mc.plugin.base.module.ModuleManager;
import de.floorisjava.mzcamp.mzcamp.modules.ui.listener.ChatFormatter;
import de.floorisjava.mzcamp.mzcamp.modules.ui.listener.TabFormatter;
import net.milkbowl.vault.chat.Chat;

/**
 * Handles the UI (chat, tab, ...).
 */
public class UiModule extends BaseModule {

    /**
     * The Vault Chat API.
     */
    private Chat chat;

    /**
     * Constructor.
     *
     * @param moduleManager The owning module manager.
     */
    public UiModule(final ModuleManager moduleManager) {
        super(moduleManager);

        addListener(() -> new ChatFormatter(chat));
        addListener(() -> new TabFormatter(chat));
    }

    @Override
    protected void startModule() {
        chat = getModuleManager().getServer().getServicesManager().load(Chat.class);
    }
}
