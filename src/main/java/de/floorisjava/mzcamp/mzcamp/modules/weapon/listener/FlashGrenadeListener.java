/*
 * Copyright (C) 2020 FloorIsJava
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.mzcamp.mzcamp.modules.weapon.listener;

import de.floorisjava.mc.plugin.base.util.ItemUtil;
import de.floorisjava.mzcamp.mzcamp.CustomItem;
import lombok.RequiredArgsConstructor;
import org.bukkit.GameMode;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

/**
 * Implements ender pearl grenades.
 */
@RequiredArgsConstructor
public class FlashGrenadeListener implements Listener {

    /**
     * The owning plugin, for scheduling.
     */
    private final Plugin plugin;

    @EventHandler
    public void onPlayerInteract(final PlayerInteractEvent event) {
        if (event.getAction() != Action.RIGHT_CLICK_AIR && event.getAction() != Action.RIGHT_CLICK_BLOCK) {
            return;
        }

        if (event.getHand() != EquipmentSlot.HAND || !event.hasItem()) {
            return;
        }

        final ItemStack item = event.getItem();
        if (!CustomItem.FLASH_GRENADE.isItem(item)) {
            return;
        }

        final Item thrown = event.getPlayer().getWorld().dropItem(event.getPlayer().getEyeLocation(),
                CustomItem.FLASH_GRENADE.createStack());
        thrown.setVelocity(event.getPlayer().getLocation().getDirection());
        thrown.setPickupDelay(20 * 3600);
        plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, () -> {
            thrown.remove();
            for (final Entity entity : thrown.getNearbyEntities(6, 6, 6)) {
                if (entity instanceof Player) {
                    final Player who = (Player) entity;
                    if (who.getGameMode() == GameMode.SURVIVAL) {
                        who.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 120, 2, false, false, false));
                        who.addPotionEffect(new PotionEffect(PotionEffectType.CONFUSION, 120, 3, false, false, false));
                        who.playSound(thrown.getLocation(), Sound.BLOCK_NOTE_BLOCK_SNARE, 8.0f, 1.0f);
                        playSmokeEffect(who, 16);
                    }
                }
            }
        }, 35L);

        ItemUtil.consumeOne(item);
        event.getPlayer().getInventory().setItemInMainHand(item);
    }

    /**
     * Plays a smoke effect for the given player.
     *
     * @param who       The player.
     * @param remaining The number of remaining instances of the effect.
     */
    private void playSmokeEffect(final Player who, final int remaining) {
        if (remaining == 0) {
            return;
        }

        who.getWorld().spawnParticle(Particle.EXPLOSION_HUGE, who.getEyeLocation(), 1, 0, 0, 0);
        plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin,
                () -> playSmokeEffect(who, remaining - 1), 5L);
    }
}
