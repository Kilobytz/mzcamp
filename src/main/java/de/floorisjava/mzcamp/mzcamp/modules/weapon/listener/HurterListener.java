/*
 * Copyright (C) 2020 FloorIsJava
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.mzcamp.mzcamp.modules.weapon.listener;

import de.floorisjava.mzcamp.mzcamp.CustomItem;
import lombok.RequiredArgsConstructor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;

import java.util.Collection;
import java.util.HashSet;
import java.util.UUID;

/**
 * Implements hurter functionality.
 */
@RequiredArgsConstructor
public class HurterListener implements Listener {

    /**
     * The owning plugin, for scheduling.
     */
    private final Plugin plugin;

    /**
     * The players currently being hurt.
     */
    private final Collection<UUID> beingHurt = new HashSet<>();

    @EventHandler
    public void onEntityDamageByEntity(final EntityDamageByEntityEvent event) {
        if (!(event.getEntity() instanceof Player) || !(event.getDamager() instanceof Player)) {
            return;
        }

        final ItemStack item = ((Player) event.getDamager()).getInventory().getItemInMainHand();
        if (!CustomItem.HURTER.isItem(item)) {
            return;
        }

        event.setDamage(0.0);

        if (!beingHurt.contains(event.getEntity().getUniqueId())) {
            beingHurt.add(event.getEntity().getUniqueId());
            playHurtAnimation((Player) event.getEntity(), 200);
        }
    }

    /**
     * Plays the hurt animation to the given player.
     *
     * @param who       The player.
     * @param remaining The remaining number of animations to be played.
     */
    private void playHurtAnimation(final Player who, final int remaining) {
        if (remaining == 0 || !beingHurt.contains(who.getUniqueId())) {
            beingHurt.remove(who.getUniqueId());
            return;
        }

        final int noDamageTicks = who.getNoDamageTicks();
        who.setNoDamageTicks(0);
        who.damage(0.00001);
        who.setNoDamageTicks(noDamageTicks);
        plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin,
                () -> playHurtAnimation(who, remaining - 1), 1L);
    }
}
