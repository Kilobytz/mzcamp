/*
 * Copyright (C) 2020 FloorIsJava
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.mzcamp.mzcamp.modules.weapon.listener;

import de.floorisjava.mc.plugin.base.util.PlayerUtil;
import de.floorisjava.mzcamp.mzcamp.CustomItem;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.inventory.ItemStack;

import java.util.Random;

/**
 * Implements muramasa functionality.
 */
public class MuramasaListener implements Listener {

    /**
     * A random number generator.
     */
    private final Random random = new Random();

    @EventHandler
    public void onEntityDamageByEntity(final EntityDamageByEntityEvent event) {
        if (!(event.getEntity() instanceof Player) || !(event.getDamager() instanceof Player)) {
            return;
        }

        final ItemStack item = ((Player) event.getDamager()).getInventory().getItemInMainHand();
        if (!CustomItem.MURAMASA.isItem(item)) {
            return;
        }

        if (random.nextInt(4) == 0) {
            PlayerUtil.takeFood((Player) event.getEntity(), 1);
            event.getEntity().sendMessage("§eHungry?");
        }
    }
}
