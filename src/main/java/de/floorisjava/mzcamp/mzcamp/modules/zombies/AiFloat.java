/*
 * Copyright (C) 2020 FloorIsJava
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.mzcamp.mzcamp.modules.zombies;

import com.destroystokyo.paper.entity.ai.Goal;
import com.destroystokyo.paper.entity.ai.GoalKey;
import com.destroystokyo.paper.entity.ai.GoalType;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.data.Levelled;
import org.bukkit.entity.Mob;

import java.util.EnumSet;
import java.util.Random;

/**
 * AI goal for floating.
 */
@RequiredArgsConstructor
public class AiFloat implements Goal<Mob> {

    /**
     * A random number generator.
     */
    private static final Random RANDOM = new Random();

    /**
     * The goal key for this goal.
     */
    @Setter
    private static GoalKey<Mob> goalKey;

    /**
     * The mob.
     */
    private final Mob mob;

    @Override
    public boolean shouldActivate() {
        if (mob.isInWater()) {
            final Location mobLocation = mob.getLocation();
            final Block footBlock = mobLocation.getBlock();
            if (footBlock.getType() == Material.WATER) {
                final Levelled levelled = (Levelled) footBlock.getBlockData();
                final double levelHeight = levelled.getLevel() < 8 ? 1.0 - 0.125 * levelled.getLevel() : 1.0;
                final double offsetHeight = mobLocation.getY() - footBlock.getY();

                final double totalHeight;
                if (footBlock.getRelative(BlockFace.UP).getType() == Material.WATER) {
                    totalHeight = 1 + levelHeight - offsetHeight;
                } else {
                    totalHeight = levelHeight - offsetHeight;
                }

                // 0.4D: getFluidJumpThreshold()
                return totalHeight > 0.4D;
            }
        } else {
            return mob.isInLava();
        }
        return false;
    }

    @Override
    public void tick() {
        if (!mob.isJumping()) {
            if (RANDOM.nextFloat() < 0.6f) {
                mob.setJumping(true);
            }
        }
    }

    @Override
    public GoalKey<Mob> getKey() {
        return goalKey;
    }

    @Override
    public EnumSet<GoalType> getTypes() {
        return EnumSet.of(GoalType.JUMP);
    }
}
