/*
 * Copyright (C) 2020 FloorIsJava
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package de.floorisjava.mzcamp.mzcamp.modules.zombies.listener;

import de.floorisjava.mzcamp.mzcamp.modules.zombies.ZombieManager;
import lombok.RequiredArgsConstructor;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.PigZombie;
import org.bukkit.entity.Zombie;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.CreatureSpawnEvent;

/**
 * Listens for non-plugin creature spawning.
 */
@RequiredArgsConstructor
public class NaturalSpawnFilter implements Listener {

    /**
     * Manages zombies.
     */
    private final ZombieManager zombieManager;

    @EventHandler
    public void onCreatureSpawn(final CreatureSpawnEvent event) {
        if (event.getEntityType() == EntityType.ZOMBIE) {
            zombieManager.prepareZombie((Zombie) event.getEntity());
        } else if (event.getEntityType() == EntityType.ZOMBIFIED_PIGLIN) {
            zombieManager.preparePigZombie((PigZombie) event.getEntity());
        } else if (event.getEntityType() != EntityType.ARMOR_STAND) {
            switch (event.getSpawnReason()) {
                case SPAWNER:
                case SPAWNER_EGG:
                case BUILD_WITHER:
                case BREEDING:
                case DISPENSE_EGG:
                case CUSTOM:
                case DEFAULT:
                    return;
                default:
                    event.setCancelled(true);
                    break;
            }
        }
    }
}
